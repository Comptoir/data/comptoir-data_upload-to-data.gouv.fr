# Upload data to `data.gouv.fr`

## How to upload data from Comptoir-du-Libre.org to `data.gouv.fr` ?

```bash

# Data.gouv.fr API key
set +o history         # disable command line history
DATA_GOUV_API_KEY='<YOUR_API_KEY>'
set -o history         # enable command line history

# URLs and dataset ID
DATA_GOUV_DATASET='<DATASET_ID>'
DATA_GOUV_API_URL="https://www.data.gouv.fr/api/1"
COMPTOIR_URL="https://comptoir-du-libre.org/public/export/comptoir-du-libre_export_v1.json"

# Upload data from Comptoir-du-Libre.org to `data.gouv.fr`
cd .gitlab/ci/data-gouv-upload/
./data-gouv-upload_comptoir-du-libre.sh --dataset     "${DATA_GOUV_DATASET}"  \
                                        --api-token   "${DATA_GOUV_API_KEY}"  \
                                        --api-url     "${DATA_GOUV_API_URL}"  \
                                        --src-url     "${COMPTOIR_URL}"
```

## Configure Gitlab CI

Following Gitlab CI variables must be declared in repository configuration:
- `DATA_GOUV_DATASET`  ----> Protected + Masked
- `DATA_GOUV_API_KEY`  ----> Protected + Masked
- `DATA_GOUV_API_URL`  ----> Protected
- `COMPTOIR_URL`       ----> Protected

Note:
> -  **Protected**: only exposed to protected branches or tags.
> -  **Masked**:    hidden in job logs. Must match masking [requirements](https://gitlab.adullact.net/help/ci/variables/README#mask-a-cicd-variable).
