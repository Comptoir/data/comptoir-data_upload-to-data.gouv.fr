#!/bin/bash

set -o errexit

##########################################################################################
# shellcheck disable=SC2034
TEMP=$(getopt -o cpt --long help --long api-token: --long dataset: --long api-url: --long src-url: -n 'javawrap' -- "$@")
##########################################################################################

usage() {
    echo ""
    echo " --------------------------------------------------------------------"
    echo " Upload data from Comptoir-du-Libre.org to data.gouv.fr"
    echo " --------------------------------------------------------------------"
    echo " usage: ${0} [OPTIONS]..."
    echo ""
    echo "   --dataset   <arg>   dataset ID"
    echo "   --api-token <arg>   API key"
    echo "   --api-url   <arg>   API URL"
    echo "   --src-url   <arg>   Source URL"
    echo "   --help              Display documentation."
    echo ""
    echo " --------------------------------------------------------------------"
    echo ""
    exit 2
}

# Process the parameters
if [[ "${1}" == "" ]]; then
    usage
fi

declare DATA_GOUV_API_URL=
declare DATA_GOUV_DATASET=
declare DATA_GOUV_API_KEY=
declare SRC_URL=
while true; do
    case "$1" in
    --help)
        usage
        ;;
    --dataset)
        DATA_GOUV_DATASET="$2"
        shift 2
        ;;
    --api-token)
        DATA_GOUV_API_KEY="$2"
        shift 2
        ;;
    --api-url)
        DATA_GOUV_API_URL="$2"
        shift 2
        ;;
    --src-url)
        SRC_URL="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *) break ;;
    esac
done

# Check mandatory parameters
if [[ "${DATA_GOUV_API_KEY}" == "" ]]; then
    echo ""; echo " ----> ERROR - API key is missing"
    usage
fi
if [[ "${DATA_GOUV_DATASET}" == "" ]]; then
    echo ""; echo " ----> ERROR - Dataset ID is missing"
    usage
fi
if [[ "${DATA_GOUV_API_URL}" == "" ]]; then
    echo ""; echo " ----> ERROR - API URL is missing"
    usage
fi
if [[ "${SRC_URL}" == "" ]]; then
    echo ""; echo " ----> ERROR - Source URL is missing"
    usage
fi

##########################################################################################

# Final filename
DATE=$(date +%Y.%m.%d)
DATA_GOUV_FILE_NAME="comptoir-du-libre.org-${DATE}-logiciels-libres-v1.json"   # TODO
SRC_FILE="${DATA_GOUV_FILE_NAME}"

# Request URL
REQUEST_URL="${DATA_GOUV_API_URL}/datasets/${DATA_GOUV_DATASET}/upload/"

# Display some informations
echo " -----------------------------------------------------------------------"
echo " ---> Data file:   ${DATA_GOUV_FILE_NAME}"
echo " ---> Source URL:  ${SRC_URL}"
echo " ---> API request: ${REQUEST_URL}"
echo " -----------------------------------------------------------------------"

# Download source data
##########################################################################################
if [[ -f "./${SRC_FILE}" ]]; then
    rm "./${SRC_FILE}" # remove old file
fi
CURL_EXIT_CODE=""
set +o errexit
curl --fail --silent --show-error -X GET "${SRC_URL}"  > "${SRC_FILE}"
CURL_EXIT_CODE=$?
set -o errexit

# Check if HTTP request has not failed
if [[ "${CURL_EXIT_CODE}" != 0 ]]; then
    echo "";
    echo " ---> ERROR"
    echo " ---> CURL failed to dowload data"
    echo " ---> CURL exit code: ${CURL_EXIT_CODE}"
    echo " ---> Source URL:  ${SRC_URL}"
    exit 1
fi

# check if source file is available and if data is as expected
##########################################################################################
if [[ ! -f "./${SRC_FILE}" ]]; then
    echo ""; echo " ----> ERROR - File not found: ./${SRC_FILE}"
    usage
fi
set +o errexit
DATA_CHECK=$(grep -c '"external_resources":'  "./${SRC_FILE}")  # TODO
set -o errexit
if [[ "${DATA_CHECK}" == "0" ]]; then
    echo ""; echo " ----> ERROR - Data is not valid"
    usage
fi

# Upload data to `data.gouv.fr`
##########################################################################################
CURL_EXIT_CODE=""
set +o errexit
curl   --fail                 \
       --silent               \
       --show-error           \
       --connect-timeout  25  \
       --max-time        120  \
       --header "Accept:application/json"           \
       --header "X-Api-Key:${DATA_GOUV_API_KEY}"    \
       -F "file=@./${SRC_FILE};type=application/json;filename=${DATA_GOUV_FILE_NAME}"  \
       -X POST "${REQUEST_URL}"
   CURL_EXIT_CODE=$?
####################################################################################################################
# Documentation: https://curl.se/docs/manpage.html
#    -f, --fail         Turn 4xx HTTP error code to CURL error (22 - HTTP page not retrieved)
#    -s, --silent       Silent or quiet mode. Do not show progress meter or error messages.
#    -S, --show-error   When used with -s, --silent, it makes curl show an error message if it fails.
#    --connect-timeout  Maximum time in seconds that you allow curl's connection to take.
#                       This only limits the connection phase, so if curl connects within the given period it will continue - if not it will exit.
#    --max-time 10      Maximum time, in seconds, that we allow command line to spend before curl exits with a timeout error code (28)
####################################################################################################################
set -o errexit

# Check if HTTP request has not failed
if [[ "${CURL_EXIT_CODE}" != 0 ]]; then
    echo "";
    echo " ---> ERROR"
    echo " ---> CURL failed to upload data"
    echo " ---> CURL exit code: ${CURL_EXIT_CODE}"
    exit 1
fi


