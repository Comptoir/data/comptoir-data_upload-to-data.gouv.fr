#!/bin/bash

set -o errexit

##########################################################################################
# shellcheck disable=SC2034
TEMP=$(getopt -o cpt --long help --long api-token: --long dataset: --long api-url: -n 'javawrap' -- "$@")
#TEMP=$(getopt -o cpt --long help --long api-token: --long dataset: --long api-url: --long src-path: -n 'javawrap' -- "$@")
##########################################################################################

usage() {
    echo ""
    echo " --------------------------------------------------------------------"
    echo " Upload data (CSV) from Comptoir-du-Libre.org to data.gouv.fr"
    echo " --------------------------------------------------------------------"
    echo " usage: ${0} [OPTIONS]..."
    echo ""
    echo "   --dataset   <arg>   dataset ID"
    echo "   --api-token <arg>   API key"
    echo "   --api-url   <arg>   API URL"
#    echo "   --src-path  <arg>  Source path"
    echo "   --help              Display documentation."
    echo ""
    echo " --------------------------------------------------------------------"
    echo ""
    exit 2
}

# Process the parameters
if [[ "${1}" == "" ]]; then
    usage
fi

declare DATA_GOUV_API_URL=
declare DATA_GOUV_DATASET=
declare DATA_GOUV_API_KEY=
#declare SRC_PATH=
while true; do
    case "$1" in
    --help)
        usage
        ;;
    --dataset)
        DATA_GOUV_DATASET="$2"
        shift 2
        ;;
    --api-token)
        DATA_GOUV_API_KEY="$2"
        shift 2
        ;;
    --api-url)
        DATA_GOUV_API_URL="$2"
        shift 2
        ;;
#    --src-path)
#        SRC_PATH="$2"
#        shift 2
#        ;;
    --)
        shift
        break
        ;;
    *) break ;;
    esac
done

# Check mandatory parameters
if [[ "${DATA_GOUV_API_KEY}" == "" ]]; then
    echo ""; echo " ----> ERROR - API key is missing"
    usage
fi
if [[ "${DATA_GOUV_DATASET}" == "" ]]; then
    echo ""; echo " ----> ERROR - Dataset ID is missing"
    usage
fi
if [[ "${DATA_GOUV_API_URL}" == "" ]]; then
    echo ""; echo " ----> ERROR - API URL is missing"
    usage
fi
#if [[ "${SRC_PATH}" == "" ]]; then
#    echo ""; echo " ----> ERROR - Source PATH is missing"
#    usage
#fi

##########################################################################################

# Final filename
DATE=$(date +%Y.%m.%d)
DATA_GOUV_FILE_PREFIX="comptoir-du-libre.org-${DATE}-logiciels-libres-v1"
CSV_DATA_GOUV_FILE="${DATA_GOUV_FILE_PREFIX}.csv"

# Request URL
REQUEST_URL="${DATA_GOUV_API_URL}/datasets/${DATA_GOUV_DATASET}/upload/"

# check if source file is available and if data is as expected
CSV_SRC_FILE="data/data.csv"
if [[ ! -f "./${CSV_SRC_FILE}" ]]; then
    echo ""; echo " ----> ERROR - File not found: ${CSV_SRC_FILE}"
    usage
fi


# Upload CSV file to `data.gouv.fr`
##########################################################################################
echo " -----------------------------------------------------------------------"
echo " ---> Data file:   ${CSV_DATA_GOUV_FILE}"
echo " ---> API request: ${REQUEST_URL}"
echo " -----------------------------------------------------------------------"
CURL_EXIT_CODE=""
set +o errexit
curl   --fail                 \
       --silent               \
       --show-error           \
       --connect-timeout  25  \
       --max-time        120  \
       --header "Accept:application/json"           \
       --header "X-Api-Key:${DATA_GOUV_API_KEY}"    \
       -F "file=@./${CSV_SRC_FILE};type=text/csv;filename=${CSV_DATA_GOUV_FILE}"  \
       -X POST "${REQUEST_URL}"
   CURL_EXIT_CODE=$?
set -o errexit

# Check if HTTP request has not failed
if [[ "${CURL_EXIT_CODE}" != 0 ]]; then
    echo "";
    echo " ---> ERROR"
    echo " ---> CURL failed to upload data"
    echo " ---> CURL exit code: ${CURL_EXIT_CODE}"
    exit 1
fi
