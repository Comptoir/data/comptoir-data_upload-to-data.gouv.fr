import pandas
import requests
import json
import csv
import os

# Load JSON data
URL = "https://comptoir-du-libre.org/public/export/comptoir-du-libre_export_v1.json"
response = requests.get(URL)
jsonData = json.loads(response.text)

# Save JSON file
if not os.path.exists("output/"):
    os.mkdir('output')
f = open("output/data.json", "w")
f.write(response.text)
f.close()

# Convert JSON to CSV
data = []
for software in jsonData["softwares"]:
    wikidataId = ''
    wikidataUrl = ''
    sillId = ''
    sillUrl = ''
    framalibreSlug = ''
    framalibreUrl = ''
    cnllId = ''
    cnllUrl = ''
    externalResources = software["external_resources"];
    if type(externalResources["wikidata"]) is dict :
        wikidataId = externalResources["wikidata"]['id']
        wikidataUrl = externalResources["wikidata"]['url']
    if type(externalResources["sill"]) is dict :
        sillId = externalResources["sill"]['id']
        sillUrl = externalResources["sill"]['url']
    if type(externalResources["framalibre"]) is dict :
        framalibreSlug = externalResources["framalibre"]['slug']
        framalibreUrl = externalResources["framalibre"]['url']
    if type(externalResources["cnll"]) is dict :
        cnllId = externalResources["cnll"]['id']
        cnllUrl = externalResources["cnll"]['url']
    data.append([
        software["id"],
        software["name"],
        software["license"],
        wikidataId,
        sillId,
        cnllId,
        framalibreSlug,
        externalResources["website"],
        externalResources["repository"],
        software["url"],
        wikidataUrl,
        sillUrl,
        cnllUrl,
        framalibreUrl,
    ])
headers=[
    'comptoir_id',
    'software',
    'license',
    'wikidata_id',
    'sill_id',
    'cnll_id',
    'framalibre_slug',
    'software_website',
    'software_repository',
    'comptoir_url',
    'wikidata_url',
    'sill_url',
    'cnll_url',
    'framalibre_url',
]
df = pandas.DataFrame(data,columns=headers) # df.head(5)
csv_string = df.to_csv(
    "output/data.csv",
    index=False,
    sep=';',
    encoding='utf-8',
    quotechar='"',
    quoting=csv.QUOTE_NONNUMERIC
) # print(csv_string)

