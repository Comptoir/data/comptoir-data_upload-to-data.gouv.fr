# CI ~ Prerequisites to use CURL

Docker image of prerequisites to use `CURL`

## To update Docker image

### Edit Dockerfile

Edit [Dockerfile](Dockerfile) according to your needs
and in addition you must to change in this file `LABEL version="1.0.0"`
following [semantic versioning](http://semver.org/) recommendations:

 ```shell script
 MAJOR.MINOR.PATCH
    # MAJOR ---> a breaking change (incompatible API changes)
    # MINOR ---> add a new feature
    # PATCH ---> fix a bug
```

```dockerfile
FROM debian:bullseye-slim
LABEL version="1.0.0"  \
      description="Prerequisites to use CURL"
```

### Build Docker image and push to container registry

Build new Docker image and push to [Gitlab container registry](https://gitlab.adullact.net/demarches-simplifiees/adullact/ds-monitoring/container_registry)

```bash
# Configure variables
# in particular DOCKER_IMAGE_VERSION
# which must be identical to LABEL.version in Dockerfile
GITLAB_URI="gitlab.adullact.net:4567"
GITLAB_REPOSITORY="demarches-simplifiees/adullact/ds-monitoring"
DOCKER_IMAGE_NAME="debian-curl"
DOCKER_IMAGE_VERSION="v1.0.0" # must be identical to LABEL.version in Dockerfile

# Login to Gitlab
docker login "${GITLAB_URI}"

# Build new Docker image
docker build -t "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}" .
docker images | grep "${DOCKER_IMAGE_NAME}"

# Push to Gitlab container registry
docker push "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}"

# Logout to remove Gitlab credentials from $HOME/.docker/config.json file
docker logout "${GITLAB_URI}"
```


