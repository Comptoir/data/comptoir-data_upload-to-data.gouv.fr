# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

----------------------------

## v1.0.0  - 2022-12-20

### Added

- ci:    add job to upload data from Comptoir-du-Libre.org to `data.gouv.fr`
- ci:    add Docker image of prerequisites to use CURL
- ci:    add job to check that internal and external URLs in Markdown files are alive
- ci:    add minimal Gitlab CI (hadolint, markdownlint, yamllint and ShellCheck)
- docs:  add base files (README, LICENSE, CHANGELOG, ...)
- chore: add technical files (.gitinore, .editorconfig, ...)


----------------------------

## Template

```markdown
## Unreleased yet ---> 1.0.0  - 2023-0x-xx


### Added


### Changed


### Fixed


### Deprecated


### Security
```
