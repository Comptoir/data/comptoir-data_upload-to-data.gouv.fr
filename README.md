# Upload data from Comptoir-du-Libre.org to `data.gouv.fr`

Data from [Comptoir-du-Libre.org][1] are published on [data.gouv.fr][2]

[1]: https://comptoir-du-libre.org/fr
[2]: https://www.data.gouv.fr/fr/datasets/logiciels-libres-sur-le-comptoir-du-libre-org/

## How it works

- a scheduled CI Gitlab task:  [`job.data-gouv-upload.gitlab-ci.yml`][3]
- a bash script to upload data:  [`data-gouv-upload_comptoir-du-libre.sh`][4]
- data are uploaded [each day at 10pm via Gitlab CI][5].

[3]: .gitlab/ci/job.data-gouv-upload.gitlab-ci.yml
[4]: .gitlab/ci/data-gouv-upload/data-gouv-upload_comptoir-du-libre.sh
[5]: https://gitlab.adullact.net/Comptoir/data-gouv-upload/-/pipelines?page=1&scope=all&source=schedule

## Documentation

- [How to upload data from Comptoir-du-Libre.org to `data.gouv.fr`?](.gitlab/ci/data-gouv-upload)
- [Configure Gitlab CI](.gitlab/ci/data-gouv-upload#configure-gitlab-ci)

## License

[AGPL v3](LICENSE)
